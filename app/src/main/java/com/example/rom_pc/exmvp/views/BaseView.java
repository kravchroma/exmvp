package com.example.rom_pc.exmvp.views;

import android.content.Context;

/**
 * Created by ROM_PC on 25.11.2017.
 */

public interface BaseView {
    Context getContext();
}
