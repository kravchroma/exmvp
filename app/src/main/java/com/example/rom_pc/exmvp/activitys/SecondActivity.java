package com.example.rom_pc.exmvp.activitys;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.rom_pc.exmvp.R;
import com.example.rom_pc.exmvp.presenters.BasePresenter;
import com.example.rom_pc.exmvp.presenters.SecondPresenter;
import com.example.rom_pc.exmvp.views.SecondView;

public class SecondActivity extends AppCompatActivity
        implements SecondView {

    SecondPresenter presenter = SecondPresenter.create();
    TextView textView;
    EditText editTextA;
    EditText editTextB;
    Button button;

    public static Intent instantiate(Context context) {
        return new Intent(context, SecondActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        presenter.onCreate(this);

        textView = (TextView)findViewById(R.id.textView);
        editTextA = (EditText)findViewById(R.id.editTextA);
        editTextB = (EditText)findViewById(R.id.editTextB);
        button = (Button)findViewById(R.id.button);

        button.setOnClickListener(this::onClickSum);
    }

    private void onClickSum(View v) {
        presenter.sum(editTextA.getText().toString(), editTextB.getText().toString());
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSetResult(String text) {
        textView.setText("R = " + text);
    }

    @Override
    public void setOnError(String text) {
        textView.setText(text);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
