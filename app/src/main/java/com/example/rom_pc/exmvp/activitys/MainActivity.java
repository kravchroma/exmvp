package com.example.rom_pc.exmvp.activitys;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.rom_pc.exmvp.R;
import com.example.rom_pc.exmvp.presenters.MainPresenter;
import com.example.rom_pc.exmvp.views.MainView;

public class MainActivity extends AppCompatActivity
        implements MainView {

    MainPresenter presenter = MainPresenter.create();
    Button btnEnter;
    Button btnNext;
    EditText eText;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter.onCreate(this);

        btnEnter = (Button)findViewById(R.id.btn_enter);
        btnNext = (Button)findViewById(R.id.btn_next_screen);
        eText = (EditText)findViewById(R.id.et_name);
        textView = (TextView)findViewById(R.id.tv1);

        btnEnter.setOnClickListener(this::onClickBtnEnter);
        btnNext.setOnClickListener(this::onClickBtnNext);
    }

    private void onClickBtnNext(View view) {
       startActivity(SecondActivity.instantiate(this));
    }

    private void onClickBtnEnter(View view) {
        presenter.clickEnter(eText.getText().toString());
    }

    @Override
    public void onSetText1(String text) {
        textView.setText(text);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
