package com.example.rom_pc.exmvp.presenters;

import com.example.rom_pc.exmvp.models.Model1;
import com.example.rom_pc.exmvp.views.MainView;

/**
 * Created by ROM_PC on 25.11.2017.
 */

public class MainPresenter extends BasePresenter<MainView> {

    private Model1 model1 = Model1.getInstance();

    public void clickEnter(String text1) {
        String res = model1.getHello() + text1;
        getView().onSetText1(res);
    }

    public static MainPresenter create() {
        return new MainPresenter();
    }
}
