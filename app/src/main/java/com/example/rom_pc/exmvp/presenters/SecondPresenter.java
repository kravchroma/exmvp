package com.example.rom_pc.exmvp.presenters;

import com.example.rom_pc.exmvp.views.SecondView;

/**
 * Created by ROM_PC on 25.11.2017.
 */

public class SecondPresenter extends BasePresenter<SecondView> {

    public void sum(String a, String b) {
        try {
            int ia = Integer.valueOf(a);
            int ib = Integer.valueOf(b);
            int ir = ia + ib;
            getView().onSetResult(String.valueOf(ir));
        } catch (Exception ex) {
            getView().setOnError(ex.getMessage());
        }
    }

    public static SecondPresenter create() {
        return new SecondPresenter();
    }
}
