package com.example.rom_pc.exmvp.presenters;

import com.example.rom_pc.exmvp.views.BaseView;

/**
 * Created by ROM_PC on 25.11.2017.
 */

public class BasePresenter<T extends BaseView> {

    private T view;

    public void onCreate(T view) {
        this.view = view;
    }

    public void onResume() {
    }

    public void onPause() {
    }

    public void onDestroy() {
    }

    public void onDestroyView() {
    }

    public void onStart() {
    }

    public void onStop() {
    }

    public T getView() {
        return view;
    }
}
