package com.example.rom_pc.exmvp.models;

/**
 * Created by ROM_PC on 25.11.2017.
 */

public class Model1 {

    private String text = "Hello ";

    private Model1() {

    }

    public String getHello() {
        return text;
    }

    private static class SingletonHolder {
        static final Model1 HOLDER_INSTANCE = new Model1();
    }

    public static  Model1 getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }
}
