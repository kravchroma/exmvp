package com.example.rom_pc.exmvp.views;

/**
 * Created by ROM_PC on 25.11.2017.
 */

public interface SecondView extends BaseView {

    void onSetResult(String text);
    void setOnError(String text);
}
